# tw-mybatis-coding-test

This is a coding test for tw backend developer candidates.

#Objective
Use [Mybatis Mapper](http://www.mybatis.org/mybatis-3) to write a basic CRUD application on a User table(users.sql) against a postgres database.

# Instructions to code
*  This repo is readonly, so fork this repo on gitlab and create new repo with public access
*  use spring-boot and maven to give structure to the application
*  use mybatis code-generator to auto-generate the mapping code
*  use junit to write a basic test suite for the CRUD operations
*  update the readme to add accurate instructions on how to setup and run the project and run the tests
*  Share the forked repo link with the hr

#Instructions to run 
TODO by candidate